#include <stdio.h>

#include "runner.h"
#include "bullet.h"
#include "cmd.h"

/* forward declarations */
static void run_barrage(runner_t * r, barrage_t * b);
static void run_action(runner_t * r, action_t * a);
static void run_bullet(runner_t * r, bullet_t * b);
static void run_fire(runner_t * r, fire_t * f);

float get_direction(runner_t * r, direction_t * d) {
    float dir;
	type_t type;

    bool is_default = TRUE;
    dir = d->formula();

    if (d->type != TYPE_NONE) {
		type = d->type;
		is_default = FALSE;

		switch(type) {
		case TYPE_ABSOLUTE:
			if (SCROLL_TYPE_HORIZONTAL == r->barrage->scroll) {
				dir -= 90;
			}
			break;
		case TYPE_RELATIVE:
			dir += r->get_bullet_direction();
			break;
		case TYPE_SEQUENCE:
			if (!r->prev_dir->is_valid) {
				dir = 0;
				is_default = TRUE;
			}
			else dir += r->prev_dir->value;
		default:
			is_default = TRUE;
			break;
		}

    }

    if (is_default) {
		dir += r->get_aim_direction();
    }

    while (dir > 360) dir -= 360;
    while (dir < 0) dir += 360;

    r->prev_dir->value = dir;

    return dir;
}

float get_speed(runner_t * r, speed_t* s) {
    float spd;
	type_t type;

    spd = s->formula();
    if (s->type != TYPE_NONE) {
		type = s->type;

		switch(type) {
		case TYPE_RELATIVE:
			spd += r->get_bullet_speed();
			break;
		case TYPE_SEQUENCE:
			if (!r->prev_spd->is_valid) {
				spd = 1;
			}
			else {
				spd += r->prev_spd->value;
			}
			break;
		default:
			break;
		}
    }

    r->prev_spd->value = spd;

    return spd;
}

void run_change_direction(runner_t * r) {
	direction_t * dir_node = fetch_direction(r->current_node);
	int term = fetch_term(r->current_node);
	type_t type = dir_node->type;
	float dir;
	bool sequence = FALSE;

	if (type != TYPE_SEQUENCE) {
		dir = get_direction(dir_node);
	}
	else {
		dir = dir_node->formula();
		sequence = TRUE;
	}

	calc_change_direction(dir, term, sequence);

	r->current_node = NULL;
}


static void run_barrage(runner_t * r, barrage_t * b) {
	if (b) {
		if (b->action) {
			printf("action is not null\n");
			run_action(r, b->action);
		}
		if (b->bullet) {
			printf("bullet is not null\n");
			run_bullet(r, b->bullet);
		}
		if (b->fire) {
			printf("fire is not null\n");
			run_fire(r, b->fire);
		}
	}
}

static void run_action(runner_t * r, action_t * a) {
	if (a) {
		printf("action is not null\n");
		if (a->fire) {
			run_fire(r, a->fire);
		}
		if (a->action) {
			run_action(r, a->action);
		}
	}
}

static void run_bullet(runner_t * r, bullet_t * b) {
	if (b) {
		printf("bullet is not null\n");
		if (b->action) {
			create_bullet(r);
		}
		else {
			create_simple_bullet(r);
		}
	}
}

static void run_fire(runner_t * r, fire_t * f) {
	if (f) {
		printf("fire is not null\n");
		if (f->bullet) {
			run_bullet(r, f->bullet);
		}
	}
}

void run(barrage_t * b) {
	runner_t r;

	run_barrage(&r, b); 
}

void calc_change_direction(runner_t * r, float direction, int term, bool seq) {
	int finalTurn = actTurn_ + term;

	float dir_first = r->get_bullet_direction();

	if (seq) {
		auto_ptr_copy(changeDir_, new LinearFunc<int, double>
					  (actTurn_, finalTurn,
					   dirFirst, dirFirst + direction * term));
	}
	else {
		float dirSpace;

		float dirSpace1 = direction - dirFirst;
		float dirSpace2;
		if (dirSpace1 > 0) dirSpace2 = dirSpace1 - 360;
		else dirSpace2 = dirSpace1 + 360;
		if (fabs(dirSpace1) < fabs(dirSpace2)) dirSpace = dirSpace1;
		else dirSpace = dirSpace2;

		auto_ptr_copy(changeDir_, new LinearFunc<int, double>
					  (actTurn_, finalTurn, dirFirst, dirFirst + dirSpace));
	}
}

/*

void calc_change_speed(float speed, speed, int term) {
	int finalTurn = actTurn_ + term;

	float spdFirst = runner_->getBulletSpeed();

	auto_ptr_copy(changeSpeed_, new LinearFunc<int, double>
				  (actTurn_, finalTurn, spdFirst, speed));
}

void calc_accel_x(float vertical, int term, BulletMLNode::Type type);
void calc_accel_y(float horizontal, int term, BulletMLNode::Type type);



void run_bullet() {

}

void run_action() {

}

void run_fire() {
	shot_init();

	set_speed();
	set_direction();

}

void run_change_speed() {

}

void run_accel() {

}

void run_wait() {

}

void run_repeat() {

}

void run_vanish() {

}

*/