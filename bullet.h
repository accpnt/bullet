#ifndef __BULLET_H__
#define __BULLET_H__

/* forward declarations */
typedef float (*formula_t)(void);
typedef struct node node_t;

/* enum and struct declarations */
typedef enum {
    TYPE_NONE = 0,
    TYPE_AIM,
    TYPE_ABSOLUTE,
    TYPE_RELATIVE,
    TYPE_SEQUENCE,
    TYPE_VERTCIAL,
    TYPE_HORIZONTAL,
    TYPE_MAX
} type_t;

typedef enum {
    NODE_TYPE_NONE = 0,
    NODE_TYPE_BULLET,
    NODE_TYPE_BULLET_REF,
    NODE_TYPE_ACTION,
    NODE_TYPE_ACTION_REF,
    NODE_TYPE_FIRE,
    NODE_TYPE_FIRE_REF,
    NODE_TYPE_CHANGE_DIRECTION,
    NODE_TYPE_CHANGE_SPEED,
    NODE_TYPE_ACCEL,
    NODE_TYPE_WAIT,
    NODE_TYPE_VANISH,
    NODE_TYPE_REPEAT,
    NODE_TYPE_DIRECTION,
    NODE_TYPE_SPEED, 
    NODE_TYPE_HORIZONTAL,
    NODE_TYPE_VERTICAL,
    NODE_TYPE_TERM,
    NODE_TYPE_TIMES,
    NODE_TYPE_BARRAGE,
    NODE_TYPE_MAX
} node_type_t;

typedef union {
    type_t type;
    char * label;
} attribute_t;

struct node {
    node_type_t node_type;
    attribute_t attribute;
    float content;
    formula_t formula;
    node_t * children[];
};

typedef struct {
    node_t * topnode;
    double max_rank
    double rank;
    int type;
    int frq;
} barrage_t;

#endif
