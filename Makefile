V =

ifeq ($(strip $(V)),)
	E = @echo
	Q = @
else
	E = @\#
	Q =
endif
export E Q

TARGET := bullet
SRCS   := guwange_round_2_boss_circle_fire.c walk.c main.c
OBJS   := $(SRCS:.c=.o)

CC      = gcc
WARNINGS = -Wall -pedantic
DEBUG    = -g
INCLUDES = 
CFLAGS = $(WARNINGS) $(DEBUG) $(INCLUDES) -O3 
LDFLAGS  =
LIBS     = 

.PHONY: all clean distclean
all:: ${TARGET} ${PLUGIN}

${TARGET}: ${OBJS}
	$(E) "  LINK    " $@
	$(Q) $(CC) $(OBJS) $(LIBS) $(LDFLAGS) -o $@

%.o: %.cpp
	$(E) "  CC      " $@
	$(Q) $(CC) -c $(CFLAGS) $< -o $@

clean :
	$(E) "  CLEAN"
	$(Q) -rm -f *~ ${OBJS} ${TARGET} ${PLUGIN}
