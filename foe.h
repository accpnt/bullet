#ifndef __FOE_H__
#define __FOE_H__

#include "vector.h"

/* forward declarations */
typedef struct foe foe_t;

struct foe {
    vector_t pos, vel, ppos, spos, mv;
    int d, spd;
    foe_command_t * cmd;
    float rank;
    int spc;
    int type;
    int shield;
    int cnt, color;
    int hit;

    runner_t * runner;
};

#endif
