#include <stdio.h>

#include "bullet.h"

void walk(node_t * n) {
    node_t * current = NULL;

    if (n) {
        printf("type = %d\n", n->type);

        switch(n->type) {
        case NODE_BARRAGE:
        case NODE_BULLET:
            if (!n->node) break;
        case NODE_ACTION:
        case NODE_FIRE:
        case NODE_REPEAT:
            current = *n->node;
            while(current) {
                walk(current);
                current++;
            }
            break;
        case NODE_CHANGE_DIRECTION:
            break;
        case NODE_CHANGE_SPEED:
            break;
        case NODE_ACCEL:
            break;
        case NODE_WAIT:
            break;
        case NODE_VANISH:
            break;
        case NODE_DIRECTION:
            break;
        case NODE_SPEED:
            break;
        case NODE_HORIZONTAL:
            break;
        case NODE_VERTICAL:
            break;
        case NODE_TERM:
            break;
        case NODE_TIMES:
            break;
        default:
            printf("unsupported node type\n");
            break;
        }
    }
    else {
        printf("leaf\n");
    }
}
