
#include <stdlib.h>

#include "bullet.h"

extern float rank;


node_t guwange_round_2_boss_circle_fire = {
    .node_type = NODE_BARRAGE,
    .attribute = TYPE_VERTICAL,
    .content = TYPE_NONE,
    .formula = NULL,
    .children = &top
};


node_t circle_direction = {
    .node_type = NODE_TYPE_DIRECTION,
    .attribute = NULL,
    .content = TYPE_NONE,
    .formula = NULL,
    .children = &top
};


node_t circle_speed = {
    .node_type = NODE_TYPE_SPEED,
    .attribute = NULL,
    .content = TYPE_NONE,
    .formula = NULL,
    .children = &top
};


node_t circle_bullet = {
    .node_type = NODE_TYPE_BULLET,
    .attribute = NULL,
    .content = TYPE_NONE,
    .formula = NULL,
    .children = &top
};


node_t circle_fire = {
    .node_type = NODE_TYPE_FIRE,
    .attribute = circle,
    .content = TYPE_NONE,
    .formula = NULL,
    .children = { &circle_direction, &circle_speed, &circle_bullet, NULL }
};


node_t action = {
    .node_type = NODE_TYPE_ACTION,
    .attribute = NULL,
    .content = TYPE_NONE,
    .formula = NULL,
    .children = &toplabel
};


node_t bullet = {
    .node_type = NODE_TYPE_BULLET,
    .attribute = TYPE_NONE,
    .content = TYPE_NONE,
    .formula = NULL,
    .children = { &action, NULL }
};


node_t action = {
    .node_type = NODE_TYPE_ACTION,
    .attribute = TYPE_NONE,
    .content = TYPE_NONE,
    .formula = NULL,
    .children = { NULL }
};


node_t direction = {
    .node_type = NODE_TYPE_DIRECTION,
    .attribute = NULL,
    .content = TYPE_NONE,
    .formula = NULL,
    .children = &top
};


node_t speed = {
    .node_type = NODE_TYPE_SPEED,
    .attribute = NULL,
    .content = TYPE_NONE,
    .formula = NULL,
    .children = &top
};


node_t bullet = {
    .node_type = NODE_TYPE_BULLET,
    .attribute = NULL,
    .content = TYPE_NONE,
    .formula = NULL,
    .children = &top
};


node_t fire = {
    .node_type = NODE_TYPE_FIRE,
    .attribute = TYPE_NONE,
    .content = TYPE_NONE,
    .formula = NULL,
    .children = { &direction, &speed, &bullet, NULL }
};


node_t bullet = {
    .node_type = NODE_TYPE_BULLET,
    .attribute = TYPE_NONE,
    .content = TYPE_NONE,
    .formula = NULL,
    .children = { NULL }
};


node_t fire_circle_action = {
    .node_type = NODE_TYPE_ACTION,
    .attribute = fireCircle,
    .content = TYPE_NONE,
    .formula = NULL,
    .children = { NULL }
};


node_t action = {
    .node_type = NODE_TYPE_ACTION,
    .attribute = TYPE_NONE,
    .content = TYPE_NONE,
    .formula = NULL,
    .children = { NULL }
};


node_t top_action = {
    .node_type = NODE_TYPE_ACTION,
    .attribute = top,
    .content = TYPE_NONE,
    .formula = NULL,
    .children = { NULL }
};

