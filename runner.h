#ifndef __RUNNER_H__
#define __RUNNER_H__


#include "bullet.h"

typedef struct runner runner_t;
typedef struct valid valid_t;

typedef int bool;
#define FALSE 0
#define TRUE 1

struct valid {
    float value; 
    bool is_valid;
};

struct runner {
    /* internal variables*/
    valid_t * spd;
    valid_t * dir;
    valid_t * prev_spd;
    valid_t * prev_dir;

    node_t * current_node;
    barrage_t * barrage;

    /* function pointers */
    float (*get_bullet_speed)(void);
    float (*get_bullet_direction)(void);
    float (*get_aim_direction)(void);
    float (*get_default_speed)(void);
    float (*get_rank)(void);
    float (*create_simple_bullet)(float direction, float speed);
    float (*create_bullet)(int state, float direction, float speed);
    int (*get_turn)(void);
    void (*do_change_direction)(float);
    void (*do_change_speed)(float);
    void (*do_accel_x)(float);
    void (*do_accel_y)(float);
    float (*get_bullet_speed_x)(void);
    float (*get_bullet_speed_y)(void);
    float (*get_rand)(void);
};

void run(barrage_t * b);

#endif