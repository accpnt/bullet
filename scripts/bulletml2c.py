import sys
import re

import xml.etree.ElementTree as ET
from pathlib import Path
import chevron


type_dict = {
    "none": "TYPE_NONE",
    "aim": "TYPE_AIM",
    "absolute": "TYPE_ABSOLUTE",
    "relative": "TYPE_RELATIVE",
    "sequence": "TYPE_SEQUENCE",
    "vertical": "TYPE_VERTICAL",
    "horizontal": "TYPE_HORIZONTAL"
}

node_type_dict = {
    "bulletml": "NODE_TYPE_BARRAGE",
    "bullet": "NODE_TYPE_BULLET",
    "action": "NODE_TYPE_ACTION",
    "fire": "NODE_TYPE_FIRE",
    "changeDirection": "NODE_TYPE_CHANGE_DIRECTION",
    "changeSpeed": "NODE_TYPE_CHANGE_SPEED",
    "accel": "NODE_TYPE_ACCEL",
    "wait": "NODE_TYPE_WAIT",
    "vanish": "NODE_TYPE_VANISH",
    "repeat": "NODE_TYPE_REPEAT",
    "direction": "NODE_TYPE_DIRECTION",
    "speed": "NODE_TYPE_SPEED",
    "horizontal": "NODE_TYPE_HORIZONTAL",
    "vertical": "NODE_TYPE_VERTICAL",
    "term": "NODE_TYPE_TERM",
    "times": "NODE_TYPE_TIMES",
    "bulletRef": "NODE_TYPE_BULLET_REF",
    "actionRef": "NODE_TYPE_ACTION_REF",
    "fireRef": "NODE_TYPE_FIRE_REF",
    "param": None,
}

template_header = """
#include <stdlib.h>

#include "bullet.h"

extern float rank;

"""

template_node = """
node_t {{{ node }}} = {
    .node_type = {{{ node_type }}},
    .attribute = {{{ attribute }}},
    .content = {{{ content }}},
    .formula = {{{ formula }}},
    .children = {{{ children }}}
};

"""

template_formula = """
float {{{ formula_name}}} (void) {
    return {{{ formula }}};
}

"""

# global list variables
formulas = []
nodes = []
actions = []
bullets = []
fires = []

def snake_case(input_str):
    output_str = re.sub(r'(?<!^)(?=[A-Z])', '_', input_str).lower()
    return output_str

def generator(outfile):

    with open(outfile, "a") as f:
        gen_header = chevron.render(template_header)
        f.write(gen_header)
        for entry in formulas:
            gen_formula = chevron.render(template_formula, entry)
            f.write(gen_formula)
        for entry in nodes:
            gen_node = chevron.render(template_node, entry)
            print(gen_node)
            f.write(gen_node)


def create_struct_name(name_list):
    struct_name = '_'.join(filter(None, name_list))
    struct_name = snake_case(struct_name)

    return struct_name

def create_pointer_str(pointer_list):
    pointer_list.append("NULL")

    if len(pointer_list) == 1:
        children_ptr_str = "{ NULL }"
    else:
        children_ptr_str = ', '.join(filter(None, pointer_list))
        children_ptr_str = "{ " + children_ptr_str + " }"

    return children_ptr_str


def dispatch_action(toplabel, element):
    global nodes
    
    children_ptr_list = []

    for subelem in element:
        tag = realtag(subelem.tag)
        name = create_struct_name([toplabel, tag])
        if tag == "repeat":
            node_dict = create_node_dict(name, node_type_dict[tag], "NULL", "TYPE_NONE", "NULL", "NULL")
            nodes.append(node_dict)
            children_ptr_list.append("&"+name)
        elif tag == "fire":
            node_dict = create_node_dict(name, node_type_dict[tag], "NULL", "TYPE_NONE", "NULL", "NULL")
            nodes.append(node_dict)
            children_ptr_list.append("&"+name)            
        elif tag == "fireRef":
            node_dict = create_node_dict(name, node_type_dict[tag], "NULL", "TYPE_NONE", "NULL", "NULL")
            nodes.append(node_dict)
            children_ptr_list.append("&"+name)
        elif tag == "changeSpeed":
            node_dict = create_node_dict(name, node_type_dict[tag], "NULL", "TYPE_NONE", "NULL", "NULL")
            nodes.append(node_dict)
            children_ptr_list.append("&"+name)
        elif tag == "changeDirection":
            node_dict = create_node_dict(name, node_type_dict[tag], "NULL", "TYPE_NONE", "NULL", "NULL")
            nodes.append(node_dict)
            children_ptr_list.append("&"+name)
        elif tag == "accel":
            node_dict = create_node_dict(name, node_type_dict[tag], "NULL", "TYPE_NONE", "NULL", "NULL")
            nodes.append(node_dict)
            children_ptr_list.append("&"+name)
        elif tag == "wait":
            node_dict = create_node_dict(name, node_type_dict[tag], "NULL", "TYPE_NONE", "NULL", "NULL")
            nodes.append(node_dict)
            children_ptr_list.append("&"+name)
        elif tag == "vanish":
            node_dict = create_node_dict(name, node_type_dict[tag], "NULL", "TYPE_NONE", "NULL", "NULL")
            nodes.append(node_dict)
            children_ptr_list.append("&"+name)
        elif tag == "action":
            dispatch_action(name, subelem)
        elif tag == "actionRef":
            node_dict = create_node_dict(name, node_type_dict[tag], "NULL", "TYPE_NONE", "NULL", "&top")
            nodes.append(node_dict)
            children_ptr_list.append("&"+name)
        else:
            print("unsupported tag for fire")

    children_ptr_str = create_pointer_str(children_ptr_list)

    return children_ptr_str

def dispatch_bullet(toplabel, element):
    global nodes
    
    children_ptr_list = []

    for subelem in element:
        tag = realtag(subelem.tag)
        name = create_struct_name([toplabel, tag])
        if tag == "direction":
            node_dict = create_node_dict(name, node_type_dict[tag], "NULL", "TYPE_NONE", "NULL", "&toplabel")
            nodes.append(node_dict)
            children_ptr_list.append("&"+name)
        elif tag == "speed":
            node_dict = create_node_dict(name, node_type_dict[tag], "NULL", "TYPE_NONE", "NULL", "&toplabel")
            nodes.append(node_dict)
            children_ptr_list.append("&"+name)            
        elif tag == "action":
            node_dict = create_node_dict(name, node_type_dict[tag], "NULL", "TYPE_NONE", "NULL", "&toplabel")
            nodes.append(node_dict)
            children_ptr_list.append("&"+name)
        elif tag == "actionRef":
            node_dict = create_node_dict(name, node_type_dict[tag], "NULL", "TYPE_NONE", "NULL", "&top")
            nodes.append(node_dict)
            children_ptr_list.append("&"+name)
        else:
            print("unsupported tag for bullet")

    children_ptr_str = create_pointer_str(children_ptr_list)

    return children_ptr_str

def dispatch_fire(toplabel, element):
    global nodes
    
    children_ptr_list = []

    for subelem in element:
        tag = realtag(subelem.tag)
        name = create_struct_name([toplabel, tag])
        if tag == "direction":
            node_dict = create_node_dict(name, node_type_dict[tag], "NULL", "TYPE_NONE", "NULL", "&top")
            nodes.append(node_dict)
            children_ptr_list.append("&"+name)
        elif tag == "speed":
            node_dict = create_node_dict(name, node_type_dict[tag], "NULL", "TYPE_NONE", "NULL", "&top")
            nodes.append(node_dict)
            children_ptr_list.append("&"+name)            
        elif tag == "bullet":
            node_dict = create_node_dict(name, node_type_dict[tag], "NULL", "TYPE_NONE", "NULL", "&top")
            nodes.append(node_dict)
            children_ptr_list.append("&"+name)
        elif tag == "bulletRef":
            node_dict = create_node_dict(name, node_type_dict[tag], "NULL", "TYPE_NONE", "NULL", "&top")
            nodes.append(node_dict)
            children_ptr_list.append("&"+name)
        else:
            print("unsupported tag for fire")

    children_ptr_str = create_pointer_str(children_ptr_list)

    return children_ptr_str

def dispatch(toplabel, element):
    global nodes

    tag = realtag(element.tag)
    print(element.attrib)

    if tag == "bulletml":
        print(nodes)
    elif tag == "bullet":
        children_ptr_str = dispatch_bullet(element.get('label'), element)
        name = create_struct_name([element.get('label'), tag])
        if element.get('label') is not None:
            attribute = element.get('label')
        else:
            attribute = "TYPE_NONE"
        node_dict = create_node_dict(name, node_type_dict[tag], attribute, "TYPE_NONE", "NULL", children_ptr_str)
        nodes.append(node_dict)
    elif tag == "action":
        children_ptr_str = dispatch_fire(element.get('label'), element)
        name = create_struct_name([element.get('label'), tag])
        if element.get('label') is not None:
            attribute = element.get('label')
        else:
            attribute = "TYPE_NONE"
        node_dict = create_node_dict(name, node_type_dict[tag], attribute, "TYPE_NONE", "NULL", children_ptr_str)
        nodes.append(node_dict)
    elif tag == "fire":
        children_ptr_str = dispatch_fire(element.get('label'), element)
        name = create_struct_name([element.get('label'), tag])
        if element.get('label') is not None:
            attribute = element.get('label')
        else:
            attribute = "TYPE_NONE"
        node_dict = create_node_dict(name, node_type_dict[tag], attribute, "TYPE_NONE", "NULL", children_ptr_str)
        nodes.append(node_dict)
    elif tag == "bulletRef":
        return
    elif tag == "actionRef":
        return
    elif tag == "fireRef":
        return
    elif tag == "param":
        if ("rank" in element.text) or ("rand" in element.text):
            print(element.text)
        return
    else:
        print("unknown tag")

def realtag(tag):
    return tag.rsplit('}', 1)[1]

def create_node_dict(node, node_type, attribute, content, formula, children):
    node_dict = {
        "node": node,
        "node_type": node_type,
        "attribute": attribute,
        "content": content,
        "formula": formula,
        "children": children
    }
    return node_dict

def parser(top, root):
    global nodes

    roottag = realtag(root.tag)

    if roottag == "bulletml":
        node_dict = create_node_dict(top, "NODE_BARRAGE", type_dict[root.get('type')], "TYPE_NONE", "NULL", "&top")
        nodes.append(node_dict)

    for element in root:
        tag = realtag(element.tag)
        
        if element.get('label') is not None:
            top = element.get('label')

        print(top, roottag, root.attrib, tag, element.attrib, element.text)

        dispatch(top, element)

        parser(roottag, element)



def main(filename):
    global nodes 
    tree = ET.parse(filename)
    root = tree.getroot()

    # create output filename
    pattern = r'[^A-Za-z0-9_]+'
    outfile = re.sub(pattern, '', Path(filename).stem).lower() + ".c"
    print(outfile)

    parser(Path(outfile).stem, root)

    print(nodes)

    generator(outfile)

    
if __name__ == "__main__":
    main(sys.argv[1])